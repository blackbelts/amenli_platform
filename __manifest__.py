# -*- coding: utf-8 -*-
{
    'name': "Amenli",
    'summary': """Amenli Agregator""",
    'description': """Aggregator """,
    'author': "Black Belts Egypt",
    'website': "www.blackbelts-egypt.com",
    'category': 'plat',
    'version': '0.1',
    'license': 'AGPL-3',
    # any module necessary for this one to work correctly
    'depends': ['base','medical','helpdesk_inherit','mail'],

    # always loaded
    'data': [
        # 'security/security.xml',
        'data/send_mail.xml',

        'security/ir.model.access.csv',

        'views/setup.xml',
        'views/menu_item.xml',
        'views/amenli_helpdesk.xml',
        'views/medical.xml'

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
     'css': ['static/css/amenli.css'],
}
