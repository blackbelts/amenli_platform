from datetime import timedelta, datetime
import xlsxwriter
from xlsxwriter.workbook import Workbook
import base64
import xlrd
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError
from odoo import api, fields, models
from collections import OrderedDict

class Brands(models.Model):
      _name='car.brands'
      _rec_name = 'brand'

      brand=fields.Char('Brand')


class Category(models.Model):
      _name = 'category.table'
      from_date=fields.Date('From',required=True)
      to_date=fields.Date('To Date',required=True)
      rule = fields.Selection([('brand', 'brand'),
                               ('price', 'price'), ],
                              'Rule Based',
                              default='brand')
      insurer=fields.Many2one('res.partner',string="Insurer")
      image_link = fields.Char('Logo Url')

      gold_discount = fields.Float('Booster')

      rate_ids=fields.One2many('rating.table','cat_id',string="Category Rate")
      benefit_ids=fields.One2many('benefit.table','cat_benefit_id',string="Category Benefit")
      @api.multi
      def price(self):
            self.env['amenli.api'].sort_medical({'dob':['1999-5-4'],'sort':'zero'})


class Rating(models.Model):
      _name = 'rating.table'
      brand = fields.Many2one('car.brands',string="Brand")
      sum_insure = fields.Float('From Sum Insured')
      sum_insure_to = fields.Float('To Sum Insured')


      cat1 = fields.Float('Category 1', digits=(12,4))
      cat2 = fields.Float('Category 2', digits=(12,4))
      cat3 = fields.Float('Category 3', digits=(12,4))
      cat_id =fields.Many2one('category.table','Cat',ondelete='cascade')


class Benefits(models.Model):
      _name = 'benefit.table'
      benefit = fields.Char(string="Benefit")
      desc = fields.Char(string="Description")
      cat1 = fields.Char('Category 1')
      cat2= fields.Char('Category 2' )
      cat3 = fields.Char('Category 3')
      cat_benefit_id = fields.Many2one('category.table', 'Cat',ondelete='cascade')



class Benefits(models.Model):
      _name = 'amenli.api'

      @api.model
      def get_price(self,data):
            if data.get('id') and data.get('price'):
                  insurer_price=[]
                  price=[]
                  features={}
                  gold=[]
                  plat = []
                  diamoand = []
                  image = {}
                  result = []

                  for rec in self.env['category.table'].search([]):
                        benefits = rec.benefit_ids.search([('cat_benefit_id','=',rec.id)])
                        gold = []
                        plat = []
                        diamoand = []
                        for benefit in benefits:
                              gold.append({benefit.benefit: benefit.cat1})
                              plat.append({benefit.benefit: benefit.cat2})
                              diamoand.append({benefit.benefit: benefit.cat3})

                        insurer_price = []
                        image = {}
                        if rec.rule=='brand':
                              for record in rec.rate_ids.search(['|',('brand','=',data.get('id')),('brand','=',False),('cat_id','=',rec.id)]):

                                    insurer_price.append({'gold':{'price':data.get('price')*record.cat1,'month_price':(((data.get('price')*record.cat1)/12)+(((data.get('price')*record.cat1)/12)*(rec.gold_discount/100))),'benefit':gold,'icon':'coins'}})
                                    insurer_price.append({'platinum':{'price':data.get('price')*record.cat2,'month_price':(((data.get('price')*record.cat2)/12)+(((data.get('price')*record.cat2)/12)*(rec.gold_discount/100))),'benefit':plat,'icon':'cubes'}})
                                    insurer_price.append({'diamond':{'price':data.get('price') * record.cat3,'month_price':(((data.get('price')*record.cat3)/12)+(((data.get('price')*record.cat3)/12)*(rec.gold_discount/100))),'benefit':diamoand,'icon':'gem'}})
                                    image['image'] = rec.image_link
                              price.append({'name': rec.insurer.name, 'plan': insurer_price, 'image': image['image']})


                        elif rec.rule=='price':
                              for record in  rec.rate_ids.search([('sum_insure', '<=', data.get('price')),('sum_insure_to', '>=', data.get('price')), ('cat_id', '>=', rec.id)]):
                                    insurer_price.append({'gold':{'price':data.get('price')*record.cat1,'month_price':(((data.get('price')*record.cat1)/12)+(((data.get('price')*record.cat1)/12)*(rec.gold_discount/100))),
                                                             'benefit': gold,'icon':'coins'}})
                                    insurer_price.append({'platinum':{'price': data.get('price') * record.cat2,
                                                                 'month_price': (((data.get('price') * record.cat2) / 12)+(((data.get('price')*record.cat2)/12)*(rec.gold_discount/100))),
                                                                 'benefit': plat,'icon':'cubes'}})
                                    insurer_price.append({'diamond':{'price': data.get('price') * record.cat3,
                                                                'month_price': (((data.get('price') * record.cat3) / 12)+(((data.get('price')*record.cat3)/12)*(rec.gold_discount/100))),
                                                                'benefit': diamoand,'icon':'gem'}})
                                    image['image'] = rec.image_link
                              price.append({'name': rec.insurer.name, 'plan': insurer_price, 'image': image['image']})


                  return price

      @api.model
      def get_lowest_price(self, data):
            sort_data = self.sort({'id': data.get('id'), 'price': data.get('price'), 'sort': 'zero'})
            lowestCompany = sort_data[0]
            lowest = {}
            lowest['name'] = lowestCompany['name']
            lowest['price'] = lowestCompany['plan'][0]['gold']['month_price']
            lowest['num_company'] = len(sort_data)
            lowest['type'] = 'gold'
            return lowest
            # if data.get('id') and data.get('price'):
            #       listRates = []
            #       listPrice = []
            #       insurer_price = {}
            #       gold = []
            #       plat = []
            #       diamoand = []
            #       lists = []
            #       # lowestRate=[]
            #       for rec in self.env['category.table'].search([]):
            #             if rec.rule == 'brand':
            #                   for record in rec.rate_ids.search(['|',('brand','=',data.get('id')),('brand','=',False),('cat_id','=',rec.id)]):
            #                         listRates.append(record.cat1)
            #                         listRates.append(record.cat2)
            #                         listRates.append(record.cat3)
            #             elif rec.rule=='price':
            #                   for record in  rec.rate_ids.search([('sum_insure', '<=', data.get('price')),('sum_insure_to', '>=', data.get('price')), ('cat_id', '>=', rec.id)]):
            #                         listRates.append(record.cat1)
            #                         listRates.append(record.cat2)
            #                         listRates.append(record.cat3)
            #             lowestRate = min(listRates)
            #             listRates = []
            #             listPrice.append({'insurer':rec.insurer.name,'price':data.get('price')*lowestRate})
            #       lowestCompany = sorted(listPrice, key=lambda i: i['price'])[0]
            #       lowestCompany['month_price'] = lowestCompany.get('price') / 12
            #       company = self.env['category.table'].search([('insurer', '=', lowestCompany.get('insurer'))])
            #       for benefit in company.benefit_ids:
            #             gold.append({benefit.benefit: benefit.cat1})
            #             plat.append({benefit.benefit: benefit.cat2})
            #             diamoand.append({benefit.benefit: benefit.cat3})
            #       for record in company.rate_ids.search([('brand', '=', data.get('id'))]):
            #             insurer_price['gold'] = {'price': data.get('price') * record.cat1,
            #                                      'month_price': (data.get(
            #                                            'price') * record.cat1) / 12, 'benefit': gold}
            #             insurer_price['platinum'] = {'price': data.get('price') * record.cat2,
            #                                          'month_price': (data.get(
            #                                                'price') * record.cat2) / 12, 'benefit': plat}
            #             insurer_price['diamond'] = {'price': data.get('price') * record.cat3,
            #                                         'month_price': (data.get(
            #                                               'price') * record.cat3) / 12,
            #                                         'benefit': diamoand}
            #
            #       return lowestCompany, insurer_price

      @api.model
      def sort(self, data):
            dataPrice = {'id':data.get('id'), 'price': data.get('price')}
            unsortedData = self.get_price(dataPrice)
            if data.get('sort') == 'zero':
                  sorted_data = sorted(unsortedData, key=lambda i: i['plan'][0]['gold']['price'])
            else:
                  sorted_data = sorted(unsortedData, key=lambda i: i['plan'][0]['gold']['price'], reverse=True)

            return sorted_data

      @api.model
      def filter(self, data):
            filterdData = []
            dataPrice = {'id': data.get('id'), 'price': data.get('price')}
            unsortedData = self.get_price(dataPrice)
            unfilterdData = sorted(unsortedData, key=lambda i: i['plan'][0]['gold']['price'])

            for key in unfilterdData:
                  if data.get('filter') == 'gold':
                        filterdData.append({'name': key['name'], 'plan': [{'gold': key['plan'][0]['gold']}],'image': key['image']})

                  elif data.get('filter') == 'platinum':
                        filterdData.append({'name': key['name'], 'plan': [{'platinum': key['plan'][1]['platinum']}],'image': key['image']})
                  elif data.get('filter') == 'diamond':
                        filterdData.append({'name': key['name'], 'plan': [{'diamond': key['plan'][2]['diamond']}],
                                            'image': key['image']})
                  else:
                        filterdData = unfilterdData

            return filterdData

      @api.model
      def get_company(self,data):
            sort_data = self.sort({'id': data.get('id'), 'price': data.get('price'), 'sort': 'zero'})
            for rec in sort_data:
                  if rec['name'] == data.get('name'):
                        result = rec
                        return result

      @api.model
      def get_plan_company(self, data):
            company = self.get_company({'id':data.get('id'),'price': data.get('price'), 'name':data.get('name')})
            plan = company['plan']
            if data.get('filter') == 'gold':
                  result = plan[0]
            elif data.get('filter') == 'platinum':
                  result = plan[1]
            elif data.get('filter') == 'diamond':
                  result = plan[2]
            else:
                  result = plan
            return result

      def calculate_age(self, DOB):
            ages = []
            for rec in DOB:
                  today = datetime.today().date()
                  DOB = datetime.strptime(rec, '%Y-%m-%d').date()
                  difference = relativedelta(today, DOB)
                  age = difference.years
                  months = difference.months
                  days = difference.days
                  if months or days != 0:
                        age += 1
                  ages.append(age)
            return ages

      @api.model
      def get_covers(self):
            res = []
            for cover in self.env['benefit.table'].search([]):
                  for rec in self.env['category.table'].search([]):
                        for covers in rec.benefit_ids:
                              if covers.benefit == cover.benefit:
                                    item = {'cover': covers.benefit, 'description': covers.desc}
                                    if item not in res:
                                          res.append(item)
            return res


      @api.model
      def get_medical_price(self,data):
            price = []

            age = data.get('dob')
            for rec in self.env['medical.price'].search([]):
                  gold = []
                  plat = []
                  diamoand = []
                  for benefit in rec.cover_lines.search([('cover_id', '=', rec.id)]):
                        gold.append({benefit.benefit:benefit.cat1})
                        plat.append({benefit.benefit:benefit.cat2})
                        diamoand.append({benefit.benefit:benefit.cat3})

                  insurer_price = []
                  image = {}
                  for record in rec.price_lines.search(
                          [('price_id', '=', rec.id)]):
                        if record.from_age <= age and record.to_age >= age:
                              insurer_price.append({'gold': {'price': record.cat1,
                                                             'month_price': record.cat1 / 12,
                                                             'benefit': gold, 'icon': 'coins'}})
                              insurer_price.append({'platinum': {'price': record.cat2,
                                                                 'month_price': record.cat2 / 12,
                                                                 'benefit': plat, 'icon': 'cubes'}})
                              insurer_price.append({'diamond': {'price': record.cat3,
                                                                'month_price': record.cat3 / 12,
                                                                'benefit': diamoand, 'icon': 'gem'}})
                  image['image'] = rec.image_link
                  price.append({'name': rec.insurer.name, 'plan': insurer_price, 'image': image['image']})
            print(price)
            return price

      @api.model
      def sort_medical(self, data):
            dataPrice = {'dob': data.get('dob')}
            unsortedData = self.get_medical_price(dataPrice)
            result = []
            if data.get('sort') == 'zero':
                  data = sorted(unsortedData, key=lambda i: i['plan'][0]['gold']['price'])
                  for key in data:
                        result.append({'name': key['name'], 'plan': [{'gold': key['plan'][0]['gold']}], 'image': key['image']})
            else:
                  data = sorted(unsortedData, key=lambda i: i['plan'][0]['gold']['price'], reverse=True)
                  for key in data:
                        result.append({'name': key['name'], 'plan': [{'gold': key['plan'][0]['gold']}], 'image': key['image']})
            return result

      @api.model
      def get_medical_lowest_price(self, data):
            sort_data = self.sort_medical({'dob': data.get('dob'), 'sort': 'zero'})
            lowestCompany = sort_data[0]
            lowest = {}
            lowest['name'] = lowestCompany['name']
            lowest['price'] = lowestCompany['plan'][0]['gold']['price']
            lowest['num_company'] = len(sort_data)
            lowest['type'] = 'gold'
            return lowest

      @api.model
      def get_medical_company(self, data):
            dataPrice = {'dob': data.get('dob')}
            unsortedData = self.get_medical_price(dataPrice)
            sort_data = sorted(unsortedData, key=lambda i: i['plan'][0]['gold']['price'])
            for rec in sort_data:
                  if rec['name'] == data.get('name'):
                        result = rec
                        return result

      @api.model
      def get_medical_plan_company(self, data):
            company = self.get_medical_company({'dob': data.get('dob'), 'name': data.get('name')})
            plan = company['plan']
            if data.get('filter') == 'gold':
                  result = plan[0]
            elif data.get('filter') == 'platinum':
                  result = plan[1]
            elif data.get('filter') == 'diamond':
                  result = plan[2]
            else:
                  result = plan
            print('reeee', result)
            return result


class amenliHelpDesk(models.Model):
    _inherit = 'helpdesk_lite.ticket'

    sum_insured = fields.Float('Sum Insured')
    brand = fields.Many2one('car.brands', string="Brand")
    age = fields.Char('Age')
    plan = fields.Char('Life Plan')


    @api.multi
    @api.model
    def send_mail_template(self, mail):
          # mail='eslam3bady@gmail.com'
          self.ensure_one()
          ir_model_data = self.env['ir.model.data']
          template_id = self.env.ref('amenli_platform.email_template_amenli')
          template_id.write({'email_to': mail})

          template_id.send_mail(self.ids[0], force_send=True)

class ticketApi(models.Model):
      _inherit = 'ticket.api'

      @api.model
      def create_ticket(self,data):
            if data.get('type') == 'medical':
                  name = 'Medical'
            elif data.get('type') == 'life':
                  name = 'Life'
            elif data.get('type') == 'medical chronic diseases':
                  name = 'Medical For Chronic Diseases'
            else:
                  name = 'Car'
            ticket_id=self.env['helpdesk_lite.ticket'].create(
                  {'name': name, 'contact_name': data.get('name'), 'phone': data.get('phone'),
                   'email_from': data.get('mail'), 'sum_insured': data.get('sum_insured'), 'brand': data.get('car'),
                   'age': data.get('age'), 'plan': data.get('plan')})

            self.env['helpdesk_lite.ticket'].search([('id', '=', ticket_id.id)]).send_mail_template(data.get('mail'))
            return ticket_id




      @api.model
      def download_policy(self):
            return {
                  'type': 'ir.actions.act_url',
                  'name': 'agents',
                  'url': 'http://207.154.195.214:8070/report/pdf/smart_travel_agency.policy/46',
                  'target': 'self'
            }
