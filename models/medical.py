from odoo import api, fields, models

class AmenliMedical(models.Model):
    _inherit = 'medical.price'

    from_date = fields.Date('From')
    to_date = fields.Date('To Date')

    insurer = fields.Many2one('res.partner', string="Insurer")
    image_link = fields.Char('Logo Url')

    @api.multi
    def price(self):
        self.env['amenli.api'].get_medical_plan_company({'dob': 20,'name':'Arope','filter':'gold'})

class AmenliMedicalPriceLine(models.Model):
    _inherit = 'medical.price.line'

    cat1 = fields.Float('Category 1')
    cat2 = fields.Float('Category 2')
    cat3 = fields.Float('Category 3')

class AmenliMedicalBenefits(models.Model):
    _inherit = 'medical.cover'

    cat1 = fields.Text(string='Category 1')
    cat2 = fields.Text(string='Category 2')
    cat3 = fields.Text(string='Category 3')

